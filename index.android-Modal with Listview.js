import React, { Component } from 'react';
import { Container, Content, List, ListItem, Thumbnail } from 'native-base';
import {Modal, TouchableHighlight, View, AppRegistry, Text} from 'react-native';

import ActionButton from 'react-native-action-button';
class DemoOne extends Component {
  constructor(props) { super(props); this.state = {modalVisible: false}; }
  setModalVisible(visible) { this.setState({modalVisible: visible}); }

  answer(){
    console.log('Answer');
  }
    render() {
        return (
          <View style = {{flex: 1}}>
            <Modal
             animationType = {"slide"}
             transparent = {false}
             visible = {this.state.modalVisible}
             onRequestClose = {() => {alert("Modal has been closed.")}}
            >
            <View style={{marginTop: 22}}>
              <View>
              <Container>
                <Content>
                    <List>
                        <ListItem>
                        <Thumbnail
                          square size = {24}
                          source = {require('./iconContainer/ok.png')}
                          />
                            <Text onPress = {this.answer}>Short Answer</Text>
                        </ListItem>
                        <ListItem>
                        <Thumbnail square size={24} source={require('./iconContainer/ok.png')} />
                            <Text>Paragraph</Text>
                        </ListItem>
                        <ListItem>
                        <Thumbnail square size={24} source={require('./iconContainer/ok.png')} />
                            <Text> Multiple Choice </Text>
                        </ListItem>
                        <ListItem>
                        <Thumbnail square size={24} source={require('./iconContainer/ok.png')} />
                            <Text>Check Boxes</Text>
                        </ListItem>
                        <ListItem>
                        <Thumbnail square size={24} source={require('./iconContainer/ok.png')} />
                            <Text>Date</Text>
                        </ListItem>
                        <ListItem>
                        <Thumbnail square size={24} source={require('./iconContainer/ok.png')} />
                            <Text>Time</Text>
                            </ListItem>
                    </List>
                </Content>
            </Container>
                <TouchableHighlight onPress={() => {
                  this.setModalVisible(!this.state.modalVisible) }}>
                    <Text>Hide Modal</Text>
                </TouchableHighlight>
              </View>
            </View>
          </Modal>
          <ActionButton
            onPress={() => {
            this.setModalVisible(true) }}
          />
          </View>
        );
    }
}
 AppRegistry.registerComponent('DemoOne', () => DemoOne);

