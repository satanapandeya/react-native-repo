'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  TouchableHighlight
} from 'react-native';


//This is wrapper class
class Test extends Component {
  renderScene(route, navigator) {
    if(route.name == 'Main') {
      return <Main navigator={navigator} {...route.passProps}  />
    }
    if(route.name == 'Home') {
      return <Home navigator={navigator} {...route.passProps}  />
    }
  } 
  
  render() {
    return (
      <Navigator
        style={{ flex:1 }}
        initialRoute={{ name: 'Main' }}
      renderScene={ this.renderScene } />
    )
  }
}
var Main = React.createClass({
  _navigate(name) {
    this.props.navigator.push({
      name: 'Home',
      passProps: {
        name: name
      }
    })
  },
  render() {    
    return (
      <View style={ styles.container }>
        <Text style={ styles.heading }>Hello from Main</Text>
        <TouchableHighlight style={ styles.button } onPress={ () => this._navigate('YOYOYOYOYO') }>
          <Text style={ styles.buttonText }>GO GO GO</Text>
        </TouchableHighlight>
      </View>
    )
  }
})

var Home = React.createClass({
  render() {    
    return (
      <View style={ styles.container }>
        <Text style={ styles.heading }>Hello from { this.props.name }</Text>
        <TouchableHighlight style={ styles.button } onPress={ () => this.props.navigator.pop() }>
          <Text style={ styles.buttonText }>GO Back</Text>
        </TouchableHighlight>
      </View>
    )
  }
})
var styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 80
  },
   heading: {
    fontSize:22,
    marginBottom:10
  },
  button: {
    height:60,
    justifyContent: 'center',
    backgroundColor: '#efefef',
    alignItems: 'center',
  },
  buttonText: {
    fontSize:20
  }
});


AppRegistry.registerComponent('Test', () => Test);

