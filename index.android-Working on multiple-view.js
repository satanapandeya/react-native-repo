'use strict';

import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

class Test extends React.Component {
  render() {
    return (
      <View style = {styles.container}>
        <View>
          <Text style = {styles.hello}>Hello, World</Text>
        </View>
        <View>
          <Text style = {styles.hello}> Hello Again </Text>
        </View>
        <View>
          <Image source = {require('./img/ic_A.png')} style = {styles.imageStyle}/>
        </View>
      </View>
    );
  }
}
var styles = StyleSheet.create({
  containerOne: {
    flex: 1,
  },
  hello: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  imageStyle:{
    height: 100,
    width:100,
    margin: 10,
  },
});
AppRegistry.registerComponent('Test', () => Test);