'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  TouchableOpacity,
} from 'react-native';
import ToolbarAndroid from 'ToolbarAndroid';
import ActionButton from 'react-native-action-button';

var SCREEN_WIDTH = require('Dimensions').get('window').width;
var BaseConfig = Navigator.SceneConfigs.FloatFromRight;

var CustomLeftToRightGesture = Object.assign({}, BaseConfig.gestures.pop, {
  // Make it snap back really quickly after canceling pop
  snapVelocity: 8,
  // Make it so we can drag anywhere on the screen
  edgeHitWidth: SCREEN_WIDTH,
});

var CustomSceneConfig = Object.assign({}, BaseConfig, {
  // A very tighly wound spring will make this transition fast
  springTension: 100,
  springFriction: 1,
  // Use our custom gesture defined above
  gestures: {
    pop: CustomLeftToRightGesture,
  }
});



var PageOne = React.createClass({
  _handlePress() {
    this.props.navigator.push({id: 2,});
  },

  render() {
    return (
      <View style={styles.container}>        
        <ToolbarAndroid style = {styles.toolbar}>
          <Text style = {styles.titleText}> Data Collector </Text>
        </ToolbarAndroid>
         <ActionButton 
           source = {require('./icon_container/ic_plus_circle_add_new_form.png')} 
           onPress = {this._handlePress}   
          >
         </ActionButton>
       </View>
    )
  },
});

var PageTwo = React.createClass({
  render() {
    return (
      <View style={styles.container}>
        <Text> Hyalo Niroj_e</Text>
        </View>
    )
  },
});

var DevForm = React.createClass({
  _renderScene(route, navigator) {
    if (route.id === 1) {
      return <PageOne navigator={navigator} />
    } else if (route.id === 2) {
      return <PageTwo navigator={navigator} />
    }
  },

  _configureScene(route) {
    return CustomSceneConfig;
  },

  render() {
    return (
      <Navigator
        initialRoute={{id: 1, }}
        renderScene={this._renderScene}
        configureScene={this._configureScene} />
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  toolbar: {
    height: 56,
    backgroundColor: '#3F51B5'
  },
  titleText: {
    color: '#fff',
  }
});

AppRegistry.registerComponent('DevForm', () => DevForm);

module.exports = DevForm;