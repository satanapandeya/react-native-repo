'use strict';

import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TextInput,
} from 'react-native';

let index = 0 
let type = ''

class Test extends Component{
  constructor(){
    super();
    this.state = {
      arr: []
    };
  }
  insertSomeThing( placeholder ){
    this.state.arr.push({index:index++, placeholder:placeholder, type: 'a'});

    this.setState({ arr: this.state.arr });
  }
  _onPressOut() {
    let temp = index ++
    this.state.arr.push(temp)
    this.setState({
        arr: this.state.arr
    })
  }

  render() {
    let arr = this.state.arr.map((r, i) => {
      if(r.type === 'a'){  
        return (
          <View key={ i } style={styles.insertStyle}>
            <TextInput 
              placeholder = {r.placeholder} 
            />
          </View>
        );
      }
      else{
        return(
          <View key = {i}> 
            <Text> Hey </Text>
          </View>
        );
      }
    });
    

    return (
        <View style={styles.container}>
        <View>
          <TouchableHighlight 
            onPress={ () => this.insertSomeThing('add') } 
            style={styles.button}>

            <Text>Add</Text>
          </TouchableHighlight>
        </View>
        <View>
          <TouchableHighlight 
            onPress={ () => this.insertSomeThing('addAgain') } 
            style={styles.button}>
               <Text>Add Again</Text>
          </TouchableHighlight>
        </View>
        <View>
           <TouchableHighlight 
              style={ styles.button } 
              onPress={ () => this._onPressOut() }>
            <Text>Push</Text>
        </TouchableHighlight>
        </View>
        { arr }
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 60,
  }, 
  gray: {
    backgroundColor: '#efefef'
  },
  insertStyle: {
    height:40,
    borderBottomColor: '#ededed',
    borderBottomWidth: 1
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    height:55,
    backgroundColor: '#ededed',
    marginBottom:10
  }
});

AppRegistry.registerComponent('Test', () => Test);