/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  StyleSheet,
  Text,
  TextInput
} from 'react-native';
// text
// textInput
class Test extends Component {
  render() {
    return (
      <View>
        <Text>I am text.</Text>
        <Text style = {styles.textFormat}>I am bold text.</Text>
        <TextInput
          style = {{height: 40}}
          placeholder = 'I am input text !!!'
        />
      </View>
    );    
  }
}
const styles = StyleSheet.create({
  textFormat:{
    fontSize: 16,
    fontWeight: 'bold',
  },
});

AppRegistry.registerComponent('Test', () => Test);

