/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';

import ToolbarAndroid from 'ToolbarAndroid';
import Prompt from 'react-native-prompt';



class DevForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      message: '',
      promptVisible: false
    };
  }

  render() {
    return (
      <View style = {styles.container}>
        <ToolbarAndroid style = {styles.toolbar}>
          <Text style = {styles.titleText}> Data Collector </Text>
        </ToolbarAndroid>

        <TouchableHighlight 
          onPress={() => this.setState({ promptVisible: true })}
        >
          <Image 
            source = {require('./icon_container/ic_plus_circle_add_new_form.png')} 
            style = {styles.addButton}       
          />
        </TouchableHighlight>
         <View style = {{ flex: 1, justifyContent: 'center' }}>
            <Text style = {{ fontSize: 20 }}>
              {this.state.message}
            </Text>
          </View>
        <Prompt
            title = "Write title of form "
            placeholder = "Your title is here"
            visible = {this.state.promptVisible}
            onCancel = {() => this.setState({ promptVisible: false, message: 'You cancel it !!!'})}
            onSubmit={(value) => this.setState({ promptVisible: false, message: `You title is "${value}"` })}/>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  toolbar: {
    height: 56,
    backgroundColor: '#3F51B5'
  },
  titleText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff'
  },
  addButton: {
    marginLeft: 270,
    marginTop: 420,
    height: 50,
    width: 50
  }
});

AppRegistry.registerComponent('DevForm', () => DevForm);

