/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  ScrollView,
  Text,
  Image,
  StyleSheet
} from 'react-native';
// Scroll View
class Test extends Component {
  render() {
    let pic = {uri: 'http://facebook.github.io/react/img/logo_og.png'};
    return (
      <ScrollView>
        <Text style = {styles.text}> This is Red Text. </Text>
        <Text style = {styles.anotherText}> This is another Text. </Text>
        <Text style = {styles.atAnotherText}> This is at anorther text. </Text>
        <Text> Image start </Text>
        <Image source = {require('./img/ic_A.png')} />
        <Image style = {styles.imageStyle} source = {pic} />
      </ScrollView>
    );    
  }
}

const styles = StyleSheet.create({
  imageStyle:{
    width: 100, 
    height: 150
  },
  text: {
    color: 'red',
    fontSize: 96,
    fontWeight: 'bold',
  },
  anotherText:{
    fontSize: 96,
    fontStyle: 'italic',
  },
  atAnotherText: {
    fontWeight: 'bold',
    fontSize: 96,
  },
});
AppRegistry.registerComponent('Test', () => Test);


