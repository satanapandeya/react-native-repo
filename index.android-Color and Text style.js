/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  StyleSheet
} from 'react-native';

class Test extends Component {
  render() {
    return (
      <View>
        <Text style = {styles.redColor}> Red </Text>
        <Text style = {styles.blueColor}> Blue </Text>
        <Text style = {styles.greenColor}> Green </Text>
        <Text>
          <Text>I am</Text>
          <Text style = {{fontWeight: 'bold'}}> reacting </Text>
          <Text> with </Text>
          <Text style = {{fontStyle: 'italic'}}> Color </Text>
          <Text> and </Text>
          <Text style = {{fontStyle: 'italic'}}> Text.</Text>
        </Text >
      </View>      
    );    
  }
}

const styles = StyleSheet.create({
  redColor: {
    color:'red',
    fontWeight: 'bold',
    fontSize: 16,
  },
  blueColor: {
    color: 'blue',
    fontSize: 18,
    fontStyle: 'italic',
  },
  greenColor: {
    color: 'green',
    fontSize: 20,
  },
});

AppRegistry.registerComponent('Test', () => Test);

