import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  AppRegistry,
  TouchableHighlight
} from 'react-native';

import CheckBox from 'react-native-icon-checkbox';


let index = 0

class Test extends Component{

    constructor(){
      super()
      this.state = {
        arr: [],
        isChecked: true,
      }
    }
    insertSomeThing(  ){
      this.state.arr.push({index:index++});
      this.setState({ arr: this.state.arr });
    }
    handlePressCheckedBox = (checked) => {
      this.setState({
        isChecked: checked,
      });
    }

    
    

    render(){
      let arr = this.state.arr.map((r, i) => {
      return (
        <View key={ i } style={styles.insertStyle}>
          <CheckBox
            size={30}
            checked={this.state.isChecked}
            onPress={this.handlePressCheckedBox}
          />
      
        </View>
      );
    });
    return(
      <View style={styles.container}>
        <View>
          <TouchableHighlight 
            onPress={ () => this.insertSomeThing('add') } 
            style={styles.button}>
            <Text>Add</Text>
          </TouchableHighlight>
        </View>
        <Text style={styles.text}>{this.state.text}</Text>
        {arr}
      </View>
    );
  }
}

let styles = StyleSheet.create({
    container: {
        marginTop: 40,
    },
    text: {
        padding: 10,
        fontSize: 14,
    },
    button: {
    alignItems: 'center',
    justifyContent: 'center',
    height:55,
    backgroundColor: '#ededed',
    marginBottom:10
  }
})


AppRegistry.registerComponent('Test', () => Test);