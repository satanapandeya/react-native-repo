import React, {Component} from 'react';
import{
  View,
  Text,
  AppRegistry
} from 'react-native';
import MultipleChoice from 'react-native-multiple-choice';

class RadioSample extends Component{
  render(){
    return(
      <View>
//https://github.com/d-a-n/react-native-multiple-choice
      <MultipleChoice
                  options={[
                      'Lorem ipsum dolor sit',
                      'Lorem ipsum',
                      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                      'Lorem ipsum dolor sit amet, consetetur',
                      'Lorem ipsum dolor'
                      ]}
                  selectedOptions={['Lorem ipsum']}
                  maxSelectedOptions={2}
                  onSelection={(option)=>alert(option + ' was selected!')}
        />
      </View>
    );
  }
}

AppRegistry.registerComponent('RadioSample', () => RadioSample);
