import React, { Component } from 'react';
import { Container, Content, List, ListItem, Thumbnail, Text } from 'native-base';
import {
  AppRegistry,
  StyleSheet,
  View,
  DrawerLayoutAndroid
} from 'react-native';
class DrawerDemo extends Component {
  render() {
    var navigationView = (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Container>
                <Content>
                    <List>
                        <ListItem>
                            <Text>Short Answer</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Paragraph</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Check Boxes</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Radio Button</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Date</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Time</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
            </View>
    );
    return (
      <DrawerLayoutAndroid drawerWidth = {180}
        drawerPosition = {DrawerLayoutAndroid.positions.Left}
        renderNavigationView = {() => navigationView}>

        <View style = {{flex: 1, alignItems: 'center'}}>
          <Text style = {{margin: 10, fontSize: 15, textAlign: 'right'}}>Hello</Text>
          <Text style = {{margin: 10, fontSize: 15, textAlign: 'right'}}>World!</Text>
        </View>
      </DrawerLayoutAndroid>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },

});

AppRegistry.registerComponent('DrawerDemo', () => DrawerDemo);
