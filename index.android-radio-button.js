/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

class RadioSample extends Component {
  constructor () {
    super()
    this.state = {

      types: [{label: 'param1', value: 0}, {label: 'param2', value: 1}, {label: 'param3', value: 2},],
      value: 0,
      valueIndex: 0,

    }
  }
  render () {
    return (
      <View style={styles.container}>
        <View style={styles.component}>
          <RadioForm
            formHorizontal={false}
            animation={true}
          >
            {this.state.types.map((obj, i) => {
              var that = this;
              var is_selected = this.state.valueIndex == i;
              return (
                <View key={i} style={styles.radioButtonWrap}>
                  <RadioButton
                    isSelected={is_selected}
                    obj={obj}
                    index={i}
                    labelHorizontal={true}
                    buttonColor={'#2196f3'}
                    labelColor={'#000'}
                    style={[i !== this.state.types.length-1 && styles.radioStyle]}
                    onPress={(value, index) => {
                      this.setState({value:value})
                      this.setState({valueIndex: index});
                    }}
                  />
                </View>
              )
            })}
          </RadioForm>
        </View>
        <Text>selected: {this.state.types[this.state.valueIndex].label}</Text>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  component: {
    marginBottom: 15,
  },
  radioStyle: {
    borderRightWidth: 1,
    borderColor: '#2196f3',
    paddingRight: 10
  },
  radioButtonWrap: {
    marginRight: 5
  },
});
AppRegistry.registerComponent('RadioSample', () => RadioSample);
